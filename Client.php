<?php

namespace ProgramBulter;

use GuzzleHttp\Client as GuzzleHttpClient;

class Client
{
    protected $config = [
        'uid' => '',
        'key' => '',
        'secret' => '',
        'host' => ''
    ];

    protected $client = null;

    public function __construct($config = [])
    {

        $this->setCofnig($config);

        if (empty($this->config['host'])) {
            throw new \Exception("请设置host", 1);
        }

        if(empty($this->config['uid'])){
            throw new \Exception("请设置软件uid", 1);
        }

        $this->client = new GuzzleHttpClient([
            "base_uri" => $this->config['host'] . '/api/Client/'
        ]);
    }

    public function setCofnig($config)
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }

    public function setKey($key)
    {
        $this->config['key'] = $key;
        return $this;
    }

    public function getKey()
    {
        return $this->config['key'];
    }
    public function setUid($uid)
    {
        $this->config['uid'] = $uid;
        return $this;
    }

    public function getUid()
    {
        return $this->config['uid'];
    }

    public function setSecret($secret)
    {
        $this->config['secret'] = $secret;
        return $this;
    }

    public function getSecret()
    {
        return $this->config['secret'];
    }


    /**
     * 申请创建一个新的站点
     *
     * @return string secret
     */
    public function postNewClient($key)
    {

        $response = $this->client->post('newClient', [
            'form_params' => [
                'key' => $key,
                'program_uid'=>$this->getUid()
            ]
        ]);

        $body = $response->getBody();

        $json_message = json_decode((string) $body,true);

        if ($json_message['code'] == 0) {
            return $json_message['data']['secret'];
        } else {
            throw new \Exception($json_message['msg'], $json_message['code']);
        }
    }

    public function getVersions($page = 1)
    {
        $response = $this->client->post('getVersions',[
            'form_params'=>$this->buildFormData([
                'uid'=>$this->getUid(),
                'page'=>$page
            ])
        ]);

        $body = $response->getBody();

        $json_message = json_decode((string)$body,true);

        if($json_message['code'] == 0){
            return $json_message['data'];
        }else{
            throw new \Exception($json_message['msg'], $json_message['code']);
        }
    }

    public function getAllVersions()
    {
        $all_list = [];
        $page = 1;
        do {
            $version_list = $this->getVersions($page);
            array_merge($all_list,$version_list);
            $page++;
        } while (count($version_list)>0);

        return $all_list;
    }

    public function getChangedFiles($uid,$page = 1)
    {
        $response = $this->client->post('getChangedFiles',[
            'form_params'=>$this->buildFormData([
                'uid'=>$uid,
                'page'=>$page
            ])
        ]);

        $body = $response->getBody();

        $json_message = json_decode((string)$body,true);

        if($json_message['code'] == 0){
            return $json_message['data'];
        }else{
            throw new \Exception($json_message['msg'], $json_message['code']);
        }
    }

    public function getAllChangedFiles($uid)
    {
        $all_list = [];
        $page = 1;
        do {
            $version_list = $this->getChangedFiles($uid,$page);
            array_merge($all_list,$version_list);
            $page++;
        } while (count($version_list)>0);

        return $all_list;
    }

    /**
     * 版本号的uid
     *
     * @param string $uid
     * @param string $save_path 存储路径,如果为null则返回数据
     * @return string 
     */
    public function getBag($uid,$save_path = null)
    {
        $response = $this->client->post('getBag',[
            'form_params'=>$this->buildFormData([
                'uid'=>$uid,
            ])
        ]);

        if($response->getHeader('content-type') == 'application/json'){
            $json_message = json_decode((string)$response->getBody(),true);
            throw new \Exception($json_message['msg']);
        }

        if(is_null($save_path)){
            return (string)$response->getBody();
        }else{
            
            $save_dir_name = dirname($save_path);

            if(!is_dir($save_dir_name)){
                mkdir($save_dir_name,0777,true);
            }

            file_put_contents($save_path,$response->getBody());
            return true;
        }
    }

    /**
     * 生成提交的form参数
     *
     * @param array $data
     * @return array
     */
    protected function buildFormData($data = [])
    {

        if (isset($data['sign'])) {
            throw new \Exception("不要试图传入sign参数", 1);
        }

        $require_data = [
            'key' => $this->getKey(),
            'timestamp' => time()
        ];


        $data = array_merge($require_data, $data);

        // 对数组的值按key排序
        ksort($data);
        // 生成url的形式
        $params = http_build_query($data);
        // 生成sign
        $sign = md5($params . $this->getSecret());

        $data['sign'] = $sign;

        return $data;
    }
}
