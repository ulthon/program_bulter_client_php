# program_bulter_client_php

#### 介绍
软件管家的客户端php版

#### 教程

##### 加载安装

```
composer require ulthon/program_bulter_client_php
```

##### 实例化

```

$client = new Client([
    'host'=>'',                 // 必要,软件中心的地址,http://ulthon.com
    'uid'=>''                   // 必要,软件的uid,
    'key'=>''                   //  站点key,可以之后设置
    'secret'=>''                // 站点秘钥,可以之后设置
]);

```

##### 新增站点

每个站点都一个单独的序列号,通过序列号请求生成一个秘钥,之后的其他接口都需要秘钥.

秘钥只能在新增时产生,没有修改接口.

你应当以可靠的方式存储`$key`和`$secret`,要达到每个站点都有一个`$key`并且不能随时更换替代.

你可以把这个信息存储到一个json文件里.

```
$key = md5(rand());             //自己生成key
$secret = $client->postpostNewClient($key);
```

##### 加载版本历史

```
$client->getVerions($uid,$page);        // 软件的uid
$client->getAllVersions($uid);          // 软件的uid
```

##### 加载包变动文件

```
$client->getChangedFiles($uid,$page);        // 版本的uid,加载版本历史得到
$client->getAllChangedFiles($uid); 
```

##### 下载软件包

```
$client->getBag($uid,$save_path);           //下载文件包,保存至某个的地方
```